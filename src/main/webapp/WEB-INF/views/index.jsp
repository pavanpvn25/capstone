<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html>
<head>
<title>CreditCard-Home Page</title>
</head>
<body>
<h2>Check for Credit Card Eligibility</h2>

<form action="checkEligibility" method="post">
<table>
<tr>
<td><label>Enter PAN Number:</label></td>
<td><input id="panNo" name="panNo" type="text" pattern="(?=.*\d)(?=.*[A-Za-z]).{10}" required="required" title="10 character alpha numeric"/></td>
</tr>
<tr>
<td><label>Enter Age (>22): </label></td>
<td><input id="age" name="age" type="number" required="required"  title="Enter Age Greater than 22" min=22></td>
</tr>
<tr>
<td><label>Enter Monthly Income (>Rs.25,000):</label></td>
<td><input id="mi" name="mi" type="number" required="required" title="Enter Monthly Income Greater than 25000" min=25000></td>
</tr>
<tr>
<td><label>Employment Type (Optional):</label></td>
 <td><select id = "myList">
               <option>Select</option>
               <option value = "Salaried">Salaried</option>
               <option value = "Selfemployed">Self-Employed</option>
             </select></td>
             </tr>
 <tr>            
<td><button id="submit" value="Check" type="submit">check</button></td>
</tr>
</table>
</form>

</body>
</html>