package com.wipro.creditcard.data;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.wipro.creditcard.model.pandetails;

@Repository
public class CreditCardDAOImpl {
	
	@Autowired
	private HibernateTemplate ht;


      public double getscore(String panNo) {
    	 
    	  pandetails pd=new pandetails();
  		try{
  			pd=ht.get(pandetails.class, panNo);
  			
  			return pd.getCreditscore();
  		}
  			catch(NullPointerException e) {
  				return -1;
  			}
		
	}
	

}
