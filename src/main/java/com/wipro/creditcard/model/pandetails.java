package com.wipro.creditcard.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import org.springframework.stereotype.Component;

@Entity
@Table
@Component
public class pandetails {
	@Id
	@Column(name="panNo")
	private String panNo;
	
	@Column(name="creditscore")
	private double creditscore;

	public pandetails() {
		
	}
	
	public String getPanNo() {
		return panNo;
	}

	public void setPanNo(String panNo) {
		this.panNo = panNo;
	}

	public double getCreditscore() {
		return creditscore;
	}

	public void setCreditscore(double creditscore) {
		this.creditscore = creditscore;
	}
	
	

}
