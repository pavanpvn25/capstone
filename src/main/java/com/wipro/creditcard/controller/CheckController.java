package com.wipro.creditcard.controller;


import com.wipro.creditcard.data.CreditCardDAOImpl;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class CheckController {
	@Autowired
	private CreditCardDAOImpl creditcarddao;
	@RequestMapping(value="/",method=RequestMethod.GET) 
	public String displayIndex()
	{
		
		return "index"; 
	}	
	
	@RequestMapping(value="/checkEligibility",method=RequestMethod.POST)
	public String pandetails(@RequestParam("panNo") String pannumber,@RequestParam("age") int age,@RequestParam("mi") int mi,
			HttpSession hs) {
		
		double eligiblescore=creditcarddao.getscore(pannumber.toUpperCase());
	
		if(eligiblescore >=5){
			hs.setAttribute("es",eligiblescore);
			return "eligible";
		}
		else if(eligiblescore >=1 && eligiblescore<=5){
			hs.setAttribute("es",eligiblescore);
			hs.setAttribute("age",age);
			hs.setAttribute("mi",mi);
			return "noteligible";
		}
		else{
			return "invalid";
		}
	}


}
